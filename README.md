# QGIS profiles for OpenLog use

This repository contains QGIS profil and deployment scenario compatible with [QGIS Deployment Toolbelt](https://guts.github.io/qgis-deployment-cli/).

----

## Quick start

### Install QDT

> It's better to use a virtual environnement.

```sh
python -m pip install -U qgis-deployment-toolbelt
```

### Launch QDT with OpenLog scenario

```sh
qgis-deployment-toolbelt -v -s qdt/openlog.qdt.yml
```

Or without repository clone:

```sh
qgis-deployment-toolbelt -v -s https://gitlab.com/geolandia/openlog/openlog-qdt-scenario/raw/main/qdt/openlog.qdt.yml
```

